#!/bin/sh

## このスクリプトを動かす前にbrewとgitをインストールしておくこと

# brew upgrade
brew tap zegervdv/zathura
brew cask install xquartz

brew install tmux zathura zathura-pdf-poppler imagemagick gibo libxml2 pwgen zsh-completions zsh-syntax-highlighting reattach-to-user-namespace the_silver_searcher go
brew install --HEAD universal-ctags/universal-ctags/universal-ctags
brew install zaquestion/tap/lab

# install git-completion.bash
echo "Install git-completion"
`curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash`

# install zsh
chsh -s /bin/zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-completions


mkdir ~/bin
(cd ~/bin/ && git clone https://github.com/neovim/neovim.git && make CMAKE_BUILD_TYPE=RelWithDebInfo && sudo make install)

# zathura
mkdir -p $(brew --prefix zathura)/lib/zathura
ln -s $(brew --prefix zathura-pdf-poppler)/libpdf-poppler.dylib $(brew --prefix zathura)/lib/zathura/libpdf-poppler.dylib

# install rbenv
echo "Install rbenv"

`brew install rbenv ruby-build`
`rbenv install 2.4.7`
`rbenv global 2.4.7`


# install pyenv
echo "Install pyenv"

`git clone git://github.com/yyuu/pyenv.git ~/.pyenv`
`git clone https://github.com/pyenv/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv`
`git clone git://github.com/yyuu/pyenv-update.git ~/.pyenv/plugins/pyenv-update`
`~/.pyenv/plugins/python-build/install.sh`
`pyenv install 3.8.2`   # for atcoder
py_new_version=`pyenv install -l | egrep "^\s*\d.\d.\d$" | tail -n 1`
`pyenv install ${py_new_version}`
`pyenv global ${py_new_version}`
`pip install --upgrade pip`
`curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python`
`source $HOME/.poetry/env`
`pip install pipenv autopep8 flake8 ipython isort yapf neovim vim-vint isort python-language-server pynvim`

go get -u install github.com/mattn/memo

# install gopls
export GO111MODULE=on
go get -u golang.org/x/tools/gopls
mkdir $HOME/.go

# install poetry
curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python

dot_files=`find . -name "_*" -maxdepth 1`
for dot_file in $dot_files;
do
    dot_file=`basename $dot_file`
    cmd="ln -sf  dotfiles/$dot_file ${HOME}/.${dot_file:1}"
    echo $cmd
    `$cmd`
done

# z command
curl https://raw.githubusercontent.com/rupa/z/master/z.sh > ~/.z.sh
