set encoding=utf-8
scriptencoding utf-8
set fileencodings=utf-8,euc-jp,iso-2022-jp,sjis

let mapleader = "\<Space>"

function! s:exists_file(filename)
  return !empty(glob(a:filename))
endfunction

" if system('uname -a | grep microsoft') != ''
"   augroup myYank
"     autocmd!
"     autocmd TextYankPost * :call system('clip.exe', @")
"   augroup END
" endif

" augroup Yank
"   au!
"   autocmd TextYankPost * :call system('clip.exe', @")
" augroup END

function s:exists_plugin(name)
    if exists('g:plugs') && has_key(g:plugs, a:name) && isdirectory(g:plugs[a:name].dir)
        return v:true
    else
        return v:false
    endif
endfunction

function! s:get_git_dir()
  let dirs =  split(expand('%:p:h'), '/')
  while dirs != []
    let dirname='/' . join(dirs, '/')
    if s:exists_file(dirname . '/.git/')
      return dirname
    endif
    call remove(dirs, -1)
  endwhile
  return v:null
endfunction
let git_dir = s:get_git_dir()

" vim-plug {{{

" if empty(glob('~/.local/share/nvim/plugged'))
"   silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
"     \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"   augroup Setting-PlugInstall
"     autocmd!
"     autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
"   augroup END
" endif
"
" " 導入Ref: http://txtbokwrm.hatenablog.com/entry/2018/08/06/165041
call plug#begin('~/.local/share/nvim/plugged')

Plug 'kana/vim-textobj-user'

Plug 'kana/vim-operator-user'

Plug 'kana/vim-textobj-indent'

Plug 'Shougo/neosnippet' " {{{
let g:neosnippet#snippets_directory = '~/.config/nvim/snippets/'
let g:neosnippet#disable_runtime_snippets = {
\ '_' : 1,
\ }

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
" }}}

Plug 'kana/vim-textobj-entire'

Plug 'glidenote/memolist.vim' " {{{
function! s:memolist_escarg(s)
  return escape(substitute(a:s, '\\', '/', 'g'), ' ')
endfunction

function! s:memolist_grep(word)
  let s:word = a:word
  if !!s:word
    let s:word = input('MGrep word: ')
  endif
  if !!s:word
    return
  endif

  let s:word = s:memolist_escarg(s:word)
  exec("call s:run_ag_in('".s:word."', '".g:memolist_path."')")
endfunction

command! -nargs=* MNew :MemoNew <args>
command! -nargs=* MList :MemoList <args>
command! -nargs=? MGrep :call s:memolist_grep(<q-args>)

nnoremap <leader>mn :<c-u>MemoNewWithMeta 'memo', '', ''<cr>
nnoremap <leader>mN :<c-u>MemoNew<cr>
nnoremap <leader>ml :<c-u>MemoList<cr>
nnoremap <leader>mg :<c-u>MGrep<cr>

let g:memolist_template_dir_path = '~/.config/nvim/memolist_template/'
if getftype(expand('~/Dropbox/')) ==? 'dir'
  let g:memolist_path = '~/Dropbox/memo/'
  nnoremap <leader>me :e ~/Dropbox/memo/
endif
let g:memolist_fzf=1
" }}}


Plug 'danro/rename.vim'

Plug 'tomtom/tcomment_vim'
if !exists('g:tcomment_types')
  let g:tcomment_types = {}
endif
let g:tcomment_types['plantuml']="' %s"
let g:tcomment_types['toml']='# %s'
let g:tcomment_types['json5']='// %s'

Plug 'lambdalisue/gina.vim'
command! -nargs=* G :Gina <args>
command! -nargs=* Ga :Gina add <args>
command! -nargs=* Gbr :Gina branch <args>
command! -nargs=* Gc :Gina commit <args>
command! -nargs=* Gd :Gina diff <args>
command! -nargs=* Gl :Gina log <args>
command! -nargs=* Ggl :Gina log --graph --oneline <args>
command! -nargs=* Gs :Gina status <args>
command! -nargs=* Gcout :Gina checkout <args>

Plug 'vim-jp/vimdoc-ja'
set helplang=ja,en

Plug 'rhysd/vim-operator-surround'
map <silent><leader>sa <Plug>(operator-surround-append)
map <silent><leader>sd <Plug>(operator-surround-delete)
map <silent><leader>sr <Plug>(operator-surround-replace)
let g:operator#surround#blocks =
\ {
\   '-' : [
\       { 'block' : ['"""', '"""'], 'motionwise' : ['char', 'line', 'block'], 'keys' : ['@'] },
\   ]
\ }

Plug 'thinca/vim-quickrun' " {{{
nnoremap <Leader>q :<C-u>bw! \[quickrun\ output\]<CR>
let quickrun_no_default_key_mappings = 0
silent! map <unique> <Leader>r <Plug>(quickrun)
let g:quickrun_config = {
\ 'plantuml': {
\   'command': 'PLANTUML',
\   'exec': ['%c %s', 'open %s:p:r.png'],
\   'outputter': 'null',
\ },
\}
let g:quickrun_config.cpp = {
\   'command': 'g++',
\   'cmdopt': '-std=c++11'
\ }
" }}}


Plug 'ntpeters/vim-better-whitespace'
let g:better_whitespace_operator=''
let g:strip_whitespace_confirm=0
let g:strip_whitespace_on_save=1
nnoremap <silent><leader>s :<c-u>StripWhitespace<cr>

Plug 'parkr/vim-jekyll'
let g:jekyll_post_template = [
\ '---',
\ 'layout: post',
\ 'title: "JEKYLL_TITLE"',
\ 'date: '.strftime('%Y-%m-%d +0900', localtime()),
\ 'tags:',
\  '-',
\ '---',
\ '']

Plug 'cocopon/vaffle.vim'
let g:vaffle_show_hidden_files = 1
nnoremap <leader>v :<c-u>Vaffle<cr>
nnoremap <leader>V :<c-u>tabnew<cr>:Vaffle<cr>
augroup Setting-Vaffle
  autocmd!
  autocmd FileType vaffle nnoremap <shift><s> <Plug>(vaffle-open-selected-split)
augroup END

Plug 'w0rp/ale' " {{{
let g:ale_linters = {
\ 'javascript': ['eslint', 'eslint-plugin-vue'],
\ 'python': ['flake8', 'pylint', 'pyright'],
\ 'ruby': ['rubocop'],
\ 'go': ['golint'],
\ 'tex': ['textlint'],
\ 'markdown': ['textlint'],
\ 'md': ['textlint'],
\ 'nim': ['nimlsp', 'nimcheck'],
\ 'css': ['stylelint'],
\ 'cpp': ['clang-format'],
\ 'vim': ['vint'],
\}
let g:ale_fixers = {
\ 'ruby': ['rubocop'],
\ 'markdown': ['textlint'],
\ 'python': ['autopep8', 'yapf', 'isort', 'black'],
\ 'nim': ['nimpretty'],
\ 'cpp': ['clang-format'],
\ 'go': ['gofmt'],
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}
let g:ale_python_auto_pipenv = 1
let g:ale_fix_on_save = 1
" call ale#Set('nim_nimpretty_options', '--maxLineLen:160')
let g:ale_nim_nimpretty_options = '--indent:4 --maxLineLen:130'

let g:ale_statusline_format = ['E%d', 'W%d', 'ok']
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
nnoremap <silent> <C-n> <Plug>(ale_next_wrap)
" }}}

Plug 'thinca/vim-template'
function! s:loaded_plugin_template()
  if search('<CURSOR>')
\   |   execute 'normal! "_da>'
\   | endif
endfunction
augroup Setting-UserTemplateLoaded
  autocmd!
  autocmd User plugin-template-loaded call s:loaded_plugin_template()
augroup END

" Plug 'nathanaelkane/vim-indent-guides' " {{{
" let g:indent_guides_exclude_filetypes = ['help', 'startify', 'fzf']
" let g:indent_guides_enable_on_vim_startup = 0
" let g:indent_guides_start_level = 2
" let g:indent_guides_guide_size = 1
" let g:indent_guides_auto_colors = 0
" augroup Setting-IndentGuide
"   autocmd!
"
"   autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=black ctermbg=white
"   autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=black ctermbg=white
" augroup END
" " }}}

Plug 'lucapette/vim-textobj-underscore'

Plug 'simeji/winresizer'
let g:winresizer_start_key='<c-s>'
let g:winresizer_vert_resize = 3
let g:winresizer_horiz_resize = 3

Plug 'kana/vim-operator-replace'
map R <Plug>(operator-replace)

Plug 'junegunn/fzf', { 'do': './install --all' }

Plug 'junegunn/fzf.vim' " {{{
nnoremap <leader>ag :Ag<space>
nnoremap <leader>Ag :exec "Ag " . expand("<cword>")<cr>
nnoremap <silent><leader>ff :<c-u>Files<CR>
nnoremap <silent><leader>gf :<c-u>GFiles<CR>
nnoremap <silent><leader>gF :<c-u>GFiles?<CR>
nnoremap <silent><leader>fh :<c-u>History<CR>
nnoremap <silent><leader>fl :<c-u>BLines<CR>

function! s:run_ag_in(pattern, path)
  let s:old_path = getcwd()
  try
    exec('cd '.a:path)
    exec('Ag '.a:pattern)
  catch
  endtry

  exec('cd '.s:old_path)
endfunction
command! -nargs=* AgIn call s:run_ag_in(<f-args>)
" }}}

Plug 'rhysd/committia.vim'

Plug 'tpope/vim-rails'

Plug 'prabirshrestha/async.vim'

Plug 'prabirshrestha/vim-lsp' " {{{
" let g:lsp_diagnostics_enabled = 0
" let g:lsp_highlight_references_enabled = 0

" debug
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('~/vim-lsp.log')
let g:asyncomplete_log_file = expand('~/asyncomplete.log')

" }}}

Plug 'prabirshrestha/asyncomplete.vim'
" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"
"
" let g:asyncomplete_smart_completion = 1
" let g:asyncomplete_auto_popup = 1

Plug 'prabirshrestha/asyncomplete-lsp.vim'

Plug 'mhinz/vim-startify' " {{{
command! -nargs=* Sfy :Startify <args>
nnoremap <silent><leader>/ :Sfy<cr>
let g:startify_custom_header = [
    \ '=======================================================',
    \ ' NeoVIM',
    \ ' hyperextensible Vim-based text editor',
    \ '=======================================================',
    \]
let g:startify_lists = [
  \ { 'type': 'files',     'header': ['   MRU']            },
  \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
  \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
\ ]

let g:startify_bookmarks = []
if s:exists_file('~/Dropbox/memo/accounts.txt')
  call add(g:startify_bookmarks, {'a': '~/Dropbox/memo/accounts.txt'})
  nnoremap <leader>na :e ~/Dropbox/memo/accounts.txt<cr>
endif
if s:exists_file('~/.config/nvim/init.vim')
  call add(g:startify_bookmarks, {'v': '~/.config/nvim/init.vim'})
  nnoremap <leader>nv :e ~/.config/nvim/init.vim<cr>
endif
if s:exists_file('~/Dropbox/memo/portfolio.markdown')
  call add(g:startify_bookmarks, {'p': '~/Dropbox/memo/portfolio.markdown'})
  nnoremap <leader>np :e ~/Dropbox/memo/portfolio.markdown<cr>
endif
if s:exists_file('~/.zshrc')
  call add(g:startify_bookmarks, {'z': '~/.zshrc'})
  nnoremap <leader>nz :e ~/.zshrc<cr>
endif
" }}}


Plug 'gutenye/json5.vim'

Plug 'mattn/webapi-vim'

Plug 'basyura/twibill.vim'

Plug 'tyru/open-browser.vim'

Plug 'h1mesuke/unite-outline'

Plug 'basyura/bitly.vim'

Plug 'Shougo/unite.vim'

Plug 'previm/previm'

" Plug 'mhinz/vim-signify'

Plug 'tpope/vim-speeddating'

Plug 'alvan/vim-closetag'
let g:closetag_filenames = '*.html,*.xhtml,*.xml,*.aiml,*.phtml,*.erb,*.php,*.vue,*.markdown'

Plug 'mattn/emmet-vim'

Plug 'Shougo/junkfile.vim'
command! -range -nargs=? Jf <line1>,<line2>call junkfile#open(
      \ strftime('junkfile_%Y%m%d_%H%M%S.'), <q-args>)
command! -nargs=* Jp :JunkfileOpen py
command! -nargs=* Jm :JunkfileOpen markdown

Plug 'kana/vim-textobj-fold'

Plug 'aklt/plantuml-syntax'

Plug 'mattn/vim-goimports'

" Plug 'deris/vim-shot-f'

Plug 'xolox/vim-misc'

Plug 'xolox/vim-session' " {{{
let g:session_directory='~/.config/nvim/sessions/'
let g:session_autoload = 'no'
let g:session_default_name = split(expand('%:p:h'), '/')[-1] + '.session'

if git_dir
  let g:session_directory = git_dir
  let g:session_default_name = '.session'
  let g:session_autosave = 'yes'
endif
" }}}

Plug 'osyo-manga/vim-textobj-multiblock'
let g:textobj_multiblock_blocks = [
	\	[ '「', '」' ],
	\	[ '（', '）' ],
  \]
omap aq <Plug>(textobj-multiblock-a)
omap iq <Plug>(textobj-multiblock-i)
vmap aq <Plug>(textobj-multiblock-a)
vmap iq <Plug>(textobj-multiblock-i)

Plug 'mattn/vim-lsp-settings'
Plug 'mattn/vim-lsp-icons'

Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'

setlocal omnifunc=lsp#complete
setlocal signcolumn=yes
nnoremap <leader>ld :LspDefinition<cr>
nnoremap <leader>lr :LspRename<cr>
inoremap <expr> <cr> pumvisible() ? "\<c-y>\<cr>" : "\<cr>"

Plug 'mattn/vim-textobj-url'

Plug 'gutenye/json5.vim'

if has('python3')
  Plug 'Shougo/deoplete.nvim'
  let g:deoplete#enable_at_startup = 1
  let g:deoplete#enable_ignore_case = 1
  " let g:deoplete#enable_smart_case = 1
endif

Plug 'roxma/nvim-yarp'

Plug 'roxma/vim-hug-neovim-rpc'

Plug 'deoplete-plugins/deoplete-jedi'

Plug 'Shougo/neco-vim'

" Plug 'sheerun/vim-polyglot'

Plug 'majutsushi/tagbar'
nnoremap <leader>tb :call tagbar#ToggleWindow()<cr>
" TODO: 左に出す

Plug 'w0ng/vim-hybrid'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme = 'minimalist'
let g:airline_section_b='%{airline#util#wrap(airline#extensions#branch#get_head(), 80)}'
let g:airline#extensions#branch#symbol=''
let g:airline_section_z='%#__accent_bold#%l%#__restore__#%#__accent_bold#/%L%{g:airline_symbols.maxlinenr}%#__restore__#:%v'
let g:airline_section_error = ''  " ステータスバーにエラーを表示しない
let g:airline_section_warning = ''  " ステータスバーに警告を表示しない

Plug 'yuttie/comfortable-motion.vim'
let g:comfortable_motion_interval = 2400.0 / 60
let g:comfortable_motion_friction = 100.0
let g:comfortable_motion_air_drag = 3.0

Plug 'machakann/vim-highlightedyank'
let g:highlightedyank_highlight_duration = 500

Plug 'posva/vim-vue' "{{{
" Ref: https://qiita.com/nabewata07/items/d92655485622aeb847a8
augroup Setting-Vue-Plug
  autocmd!
  autocmd FileType vue syntax sync fromstart
augroup END

let g:vue_ft = ''
function! NERDCommenter_before()
  if &filetype ==# 'vue'
    let g:vue_ft = 'vue'
    let stack = synstack(line('.'), col('.'))
    if len(stack) > 0
      let syn = synIDattr((stack)[0], 'name')
      if len(syn) > 0
        exe 'setf ' . substitute(tolower(syn), '^vue_', '', '')
      endif
    endif
  endif
endfunction
function! NERDCommenter_after()
  if g:vue_ft ==# 'vue'
    setf vue
    let g:vue_ft = ''
  endif
endfunction
" }}}

Plug 'AndrewRadev/switch.vim'
let g:switch_mapping = "-"

Plug 'mgedmin/python-imports.vim'

Plug 'taku-o/vim-copypath'

Plug 'dhruvasagar/vim-table-mode'

Plug 'szw/vim-tags'
let g:vim_tags_auto_generate = 1


Plug 'Shougo/deoplete.nvim'
let g:deoplete#enable_at_startup = 1

Plug 'alaviss/nim.nvim'
au User asyncomplete_setup call asyncomplete#register_source({
  \ 'name': 'nim',
  \ 'whitelist': ['nim'],
  \ 'completor': {opt, ctx -> nim#suggest#sug#GetAllCandidates({start, candidates -> asyncomplete#complete(opt['name'], ctx, start, candidates)})}
  \ })

Plug 'adi/vim-indent-rainbow'
autocmd FileType * :call rainbow#enable()

Plug 'easymotion/vim-easymotion'
let g:EasyMotion_do_mapping = 0 " Disable default mappings
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap <leader>2 <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

call plug#end()

" }}}

set background=dark
if s:exists_plugin('vim-hybrid')
  colorscheme hybrid
  highlight LineNr ctermfg=3   " 行番号を見やすく
else
  colorscheme desert
endif

" highlight CursorLine cterm=None


" filetype {{{
augroup Setting-Filetype
  autocmd!

  autocmd BufWinEnter,BufNewFile SConstruct set filetype=python
  autocmd BufNewFile,BufRead *.tex  setfiletype tex
  autocmd BufNewFile,BufRead *.py  setfiletype python
  autocmd BufNewFile,BufRead *.go  setfiletype go
  autocmd BufNewFile,BufRead *.scala  setfiletype scala
  autocmd BufNewFile,BufRead *.sage setfiletype python
  autocmd BufNewFile,BufRead *.markdown setfiletype markdown
  autocmd BufNewFile,BufRead *.plant setfiletype plantuml
  autocmd BufNewFile,BufRead *.puml setfiletype plantuml
  autocmd BufNewFile,BufRead *.iuml setfiletype plantuml
  autocmd BufNewFile,BufRead *.uml setfiletype plantuml
  autocmd BufNewFile,BufRead *.diag setfiletype blockdiag
  autocmd BufNewFile,BufRead *.max setfiletype maxima
  autocmd BufNewFile,BufRead *.gnu setfiletype gnuplot
  autocmd BufNewFile,BufRead *.md setfiletype markdown
  autocmd BufNewFile,BufRead *.mkd setfiletype markdown
  autocmd BufNewFile,BufRead *.toml setfiletype toml
augroup END
" }}}

" system {{{
set conceallevel=0
set number
set relativenumber
highlight CursorLine ctermbg=235
set visualbell t_vb=
set mouse-=a
set noequalalways
set clipboard=unnamedplus
" set clipboard+=unnamed
" let g:clipboard = {
"   \   'name': 'myClipboard',
"   \   'copy': {
"   \      '+': 'win32yank.exe -i --crlf',
"   \      '*': 'win32yank.exe -i --crlf',
"   \    },
"   \   'paste': {
"   \      '+': 'win32yank.exe -o --lf',
"   \      '*': 'win32yank.exe -o --lf',
"   \   },
"   \   'cache_enabled': 1,
" \ }
set ignorecase
set scrolloff=3
set wildmode=list:longest
set laststatus=2
set incsearch
set showcmd
set startofline

" folding
set foldmethod=marker
let &fillchars = 'vert:|,fold: '

if exists('&ambiwidth')
  set ambiwidth=double
endif
set list listchars=tab:>>,trail:.

augroup Setting-checktime
  autocmd!
  autocmd InsertEnter,WinEnter * checktime
augroup END

set noswapfile
set nobackup
set noundofile
set autoread
augroup Vimrc-Check
  autocmd!
  autocmd WinEnter * checktime
augroup END

set expandtab
set textwidth=0
set tabstop=4
set softtabstop=4
set shiftwidth=4

autocmd TermOpen * setlocal norelativenumber
autocmd TermOpen * setlocal nonumber
tnoremap <Esc> <C-\><C-n>

" vim {{{
augroup Setting-Vim
  autocmd!

  autocmd FileType vim setlocal tabstop=2
  autocmd FileType vim setlocal softtabstop=2
  autocmd FileType vim setlocal shiftwidth=2
augroup END
" }}}

" markdown {{{
augroup Setting-Markdown
  autocmd!
  autocmd FileType markdown setlocal syntax=OFF
augroup END
" }}}

" liquid {{{
augroup Setting-Liquid
  autocmd!
  autocmd FileType liquid setlocal syntax=OFF
augroup END
" }}}

" json {{{
augroup Setting-Json
  autocmd!
  autocmd FileType json setlocal conceallevel=0
augroup END
" }}}

" tex {{{
let tex_conceal = ''
augroup Setting-Tex
  autocmd!

  autocmd FileType tex setlocal textwidth=99
  autocmd FileType tex setlocal colorcolumn=100
  autocmd FileType tex setlocal tabstop=2
  autocmd FileType tex setlocal softtabstop=2
  autocmd FileType tex setlocal shiftwidth=2
augroup END
" }}}

" html {{{
augroup Setting-Html
  autocmd!

  autocmd FileType html setlocal textwidth=99
  autocmd FileType html setlocal colorcolumn=100
  autocmd FileType html setlocal tabstop=2
  autocmd FileType html setlocal softtabstop=2
  autocmd FileType html setlocal shiftwidth=2
augroup END
" }}}

" yaml {{{
augroup Setting-Yaml
  autocmd!

  autocmd FileType yaml setlocal textwidth=99
  autocmd FileType yaml setlocal colorcolumn=100
  autocmd FileType yaml setlocal softtabstop=2
  autocmd FileType yaml setlocal shiftwidth=2
augroup END
" }}}

" scala {{{
augroup Setting-Scala
  autocmd!

  autocmd FileType scala setlocal textwidth=99
  autocmd FileType scala setlocal colorcolumn=100
  autocmd FileType scala setlocal softtabstop=2
  autocmd FileType scala setlocal shiftwidth=2
  autocmd FileType scala setlocal foldmethod=syntax
augroup END
" }}}

" plantuml {{{
augroup Setting-PlantUML
  autocmd!

  autocmd FileType plantuml setlocal textwidth=99
  autocmd FileType plantuml setlocal colorcolumn=100
  autocmd FileType plantuml setlocal tabstop=2
  autocmd FileType plantuml setlocal softtabstop=2
  autocmd FileType plantuml setlocal shiftwidth=2
augroup END
" }}}

" nim {{{
augroup Setting-Nim
  autocmd!

  autocmd FileType nim setlocal textwidth=79
  autocmd FileType nim setlocal colorcolumn=100
  autocmd FileType nim setlocal tabstop=4
  autocmd FileType nim setlocal softtabstop=4
  autocmd FileType nim setlocal foldmethod=marker
  autocmd FileType nim setlocal shiftwidth=4
augroup END
" }}}

" python {{{

function! CreatePytest()
  let filenames = split(expand('%'), '/')
  let test_dirs = []
  for name in filenames[1:-2]
    let test_dirs = add(test_dirs, 'test_' . name)
  endfor
  call insert(test_dirs, 'tests')

  let tests_dirname = join(test_dirs, '/')
  call mkdir(tests_dirname, 'p')
  let tests_filename = tests_dirname . '/test_'. filenames[-1]
  execute 'edit' tests_filename
endfunction
command! Cpt :call CreatePytest()

augroup Setting-Python
  autocmd!

  if executable('pyls')
    augroup Setting-Pyls
    autocmd!
    autocmd User lsp_setup call lsp#register_server({
      \ 'name': 'pyls',
      \ 'cmd': {server_info->['pyls']},
      \ 'whitelist': ['python'],
      \ 'workspace_config': {'pyls': {'plugins': {
      \   'pycodestyle': {'enabled': v:false},
      \   'jedi_definition': {'follow_imports': v:true, 'follow_builtin_imports': v:true},}}}
    \ })
    let g:lsp_diagnostics_enabled = 0  " 警告やエラーの表示はALEに任せるのでOFFにする
  endif

   " if executable('nimlsp')
   "   autocmd User lsp_setup call lsp#register_server({
   "   \   'name': 'nimlsp',
   "   \   'cmd': {server_info->[&shell, &shellcmdflag, 'nimlsp C:\\nim\\nim-1.0.0']},
   "   \   'whitelist': ['nim'],
   "   \})
   "   autocmd FileType nim call <SID>configure_lsp()
   " endif

  autocmd FileType python setlocal tabstop=4
  " autocmd FileType python setlocal foldmethod=marker
  autocmd FileType python setlocal softtabstop=4
  autocmd FileType python setlocal shiftwidth=4
  " autocmd FileType python nnoremap <leader>a :<c-u>ALEToggle<cr>
augroup END
" }}}

" hs {{{
augroup Setting-Haskell
  autocmd!

  autocmd FileType haskell setlocal textwidth=79
  autocmd FileType haskell setlocal colorcolumn=80
  autocmd FileType haskell setlocal tabstop=2
  autocmd FileType haskell setlocal softtabstop=2
  autocmd FileType haskell setlocal shiftwidth=2
augroup END
" }}}


" blockdiag {{{
augroup Setting-Blockdiag
  autocmd!

  autocmd FileType diag setlocal textwidth=79
  autocmd FileType diag setlocal colorcolumn=80
  autocmd FileType diag setlocal tabstop=2
  autocmd FileType diag setlocal softtabstop=2
  autocmd FileType diag setlocal shiftwidth=2
augroup END
" }}}

" maxima {{{
augroup Setting-Maxima
  autocmd!

  autocmd FileType maxima setlocal textwidth=99
  autocmd FileType maxima setlocal colorcolumn=100
  autocmd FileType maxima setlocal tabstop=2
  autocmd FileType maxima setlocal softtabstop=2
  autocmd FileType maxima setlocal shiftwidth=2
augroup END
" }}}

" neosnippet {{{
augroup setting-neosnippet
  autocmd!

  autocmd filetype neosnippet setlocal noexpandtab
augroup end
" }}}
" }}}

" commands {{{
command! Dog !shiba --detach %
function! s:get_cd_target_dir()
  let l:dir = s:get_git_dir()
  if empty(l:dir)
    let l:dir = expand('%:p:h')
  endif
  return l:dir
endfunction

function! s:cd()
  exec('cd '.s:get_cd_target_dir())
endfunction
function! s:lcd()
  exec('lcd '.s:get_cd_target_dir())
endfunction
command! Cd :call s:cd()
command! Lcd :call s:lcd()
" }}}

augroup Setting-TODO
  autocmd!
  autocmd WinEnter,BufRead,BufNew,Syntax * :silent! call matchadd('Todo', '\(TODO\|NOTE\|INFO\|XXX\|TEMP\)')
  autocmd WinEnter,BufRead,BufNew,Syntax * highlight Todo guibg=Red guifg=White
augroup END
nnoremap <leader>tt :vim /TODO/<space>

set nrformats =

set autoindent
set smartindent
nnoremap J mzJ`z:delm z<cr>

let g:python_host_prog = system('echo -n $(which python2)')
let g:python3_host_prog = system('echo -n $(which python3)')

" wildignore {{{
set wildignore=*.o,*.obj,*~,*.pyc
set wildignore+=.git
set wildignore+=.tmp
set wildignore+=.coverage
set wildignore+=__pycache__/
set wildignore+=*.egg,*.egg-info
set wildignore+=*.png,*.jpg,*.gif
set wildignore+=*.pdf
" }}}

" load vimrc of local {{{
augroup vimrc-local
  autocmd!
  autocmd BufNewFile,BufReadPost * call s:vimrc_local(expand('<afile>:p:h'))
augroup END
function! s:vimrc_local(loc)
  let files = findfile('.lcvimrc', escape(a:loc, ' ') . ';', -1)
  for i in reverse(filter(files, 'filereadable(v:val)'))
    source `=i`
  endfor
endfunction
" }}}

" underline {{{
function! s:drawUnderLine(c)
  let s:l = len(getline('.')) + 2
  echo s:l
  call append('.', repeat(a:c, len(s:l) == 0? s:l : 80))
endfunction
command! -nargs=? UnderLine call s:drawUnderLine(<f-args>)
nnoremap <leader>u :<c-u>UnderLine =<cr>
nnoremap <leader>U :<c-u>UnderLine<space>
" }}}

" mapping {{{
" move by paragraph {{{
nnoremap <RIGHT> <NOP>
nnoremap <LEFT>  <NOP>
nnoremap <UP>    <NOP>
nnoremap <DOWN>  <NOP>
nnoremap L gt
nnoremap H gT
" }}}

" vimrc {{{
nnoremap <leader>. :<c-u>e ~/.config/nvim/init.vim<cr>
nnoremap <leader>. :<c-u>tabnew      ~/.config/nvim/init.vim<cr>
nnoremap <leader>tn :tabnew<cr>
nnoremap <leader>? :<c-u>source ~/.config/nvim/init.vim<cr>
nnoremap <silent><space>h :<c-u><c-u>nohlsearch<cr>
nnoremap <c-n> :cnext<cr>
nnoremap <c-p> :cprev<cr>
" }}}

" normal mode {{{
nnoremap <space><space> :<c-u>help<Space>
nnoremap <silent><c-c> o<esc>
nnoremap <silent><space>w :<c-u>write<cr>
nnoremap <silent><space>q :<c-u>quit<cr>
nnoremap <silent><space>Q :<c-u>quitall<cr>
" }}}

" folding {{{
nnoremap <c-m> za
" }}}

" tab {{{
" Ref: https://qiita.com/wadako111/items/755e753677dd72d8036d
" Anywhere SID.
function! s:SID_PREFIX()
  return matchstr(expand('<sfile>'), '<SNR>\d\+_\zeSID_PREFIX$')
endfunction

" Set tabline.
function! s:my_tabline()  "{{{
  let s = ''
  for i in range(1, tabpagenr('$'))
    let bufnrs = tabpagebuflist(i)
    let bufnr = bufnrs[tabpagewinnr(i) - 1]  " first window, first appears
    let no = i  " display 0-origin tabpagenr.
    let mod = getbufvar(bufnr, '&modified') ? '!' : ' '
    let title = fnamemodify(bufname(bufnr), ':t')
    let title = '[' . title . ']'
    let s .= '%'.i.'T'
    let s .= '%#' . (i == tabpagenr() ? 'TabLineSel' : 'TabLine') . '#'
    let s .= no . ':' . title
    let s .= mod
    let s .= '%#TabLineFill# '
  endfor
  let s .= '%#TabLineFill#%T%=%#TabLine#'
  return s
endfunction "}}}
let &tabline = '%!'. s:SID_PREFIX() . 'my_tabline()'
set showtabline=2 " 常にタブラインを表示

" The prefix key.
" Tab jump
for n in range(1, 9)
  execute 'nnoremap <silent> t'.n  ':<C-u>tabnext'.n.'<CR>'
  execute 'nnoremap <silent> t'.n  ':<C-u>tabnext'.n.'<CR>'
endfor

map <silent> tc :tablast <bar> tabnew<CR>
map <silent> tx :tabclose<CR>
map <silent> to :tabonly<CR>
map <silent> tl :tabnext<CR>
map <silent> tt :tabnext<CR>
map <silent> th :tabprevious<CR>
map <silent> tT :tabprevious<CR>
" }}}

" map {{{
nnoremap <silent>c_ ct_
nnoremap <silent>d_ dt_
nnoremap <silent>c) ct)
nnoremap <silent>d) dt)
nnoremap <silent>c} ct}
nnoremap <silent>d} dt}
nnoremap <silent>c] ct]
nnoremap <silent>d] dt]
nnoremap <silent>d' dt'
nnoremap <silent>c' ct'
nnoremap <silent>d" dt"
nnoremap <silent>c" ct"
nnoremap <silent>d. dt.
nnoremap <silent>c. ct.
nnoremap <silent>d\ d$
nnoremap <silent>D  ^d$
nnoremap <silent>c$ d$a
nnoremap <silent>c\ c$

nnoremap [q :<c-u>cprevious<CR>   " 前へ
nnoremap ]q :<c-u>cnext<CR>       " 次へ

nnoremap <space>z :<c-u>on<cr>
nnoremap <cr> za
nnoremap <leader>dd :%bdelete!<cr>
nnoremap <c-n> <c-a><c-x>
nnoremap <leader>o :only<cr>
" }}}

command! -nargs=* SnippetEdit :NeoSnippetEdit <args>
" }}}

" 行を強調表示
set cursorline
highlight CursorLine cterm=NONE ctermbg=236

" 列を強調表示
set cursorcolumn
highlight CursorColumn cterm=NONE ctermbg=236

" syntax off
