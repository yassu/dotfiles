import algorithm
import deques
import options
import posix
import sequtils
import sets
import std/heapqueue
import std/math
import std/tables
import strformat
import strutils

#{.checks: off.}
{.warning[UnusedImport]: off.}
{.hint[XDeclaredButNotUsed]: off.}

const letter: string = "abcdefghijklmnopqrstuvwxyz"
const Letter: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const MAX_INT: int = high(int)
const MIN_INT: int = low(int)
const MOD998244353 = 998_244_353
const MOD1000000007 = 1_000_000_007
const DXDY4: seq[tuple[x, y: int]] = @[(0, 1), (1, 0), (0, -1), (-1, 0)]
const DXDY8: seq[tuple[x, y: int]] = @[(0, 1), (1, 0), (0, -1), (-1, 0), (1, 1), (1, -1), (-1, -1), (-1, 1)]

proc exit() = exitnow(0)
proc scanf(formatstr: cstring){.header: "<stdio.h>", varargs.}
proc getchar(): char {.header: "<stdio.h>", varargs.}
template rep(idx: untyped, stop: SomeInteger, act: untyped): untyped =
    for idx in 0 ..< stop:
        act
template rep(x: untyped, a: openArray, act: untyped): untyped =
    for x in a:
        act
template rrep(idx: untyped, start, stop: SomeOrdinal, act: untyped): untyped =
    for idx in start ..< stop:
        act

proc nextInt(): int = scanf("%lld", addr result)
proc nextInts(n: int): seq[int] =
    var res = newSeq[int](n)
    for i in 0..<n:
        res[i] = nextInt()
    return res
# proc nextInts(h, w: int): seq[seq[int]] =
#     result = newSeqWith(h, newSeq[int](w))
#     for y in 0..<h:
#         for x in 0..<w:
#             result[y][x] = nextInt()

proc nextInt64(): int64 = scanf("%lld", addr result)
# proc nextUInt64(): uint64 = scanf("%lld", addr result)
proc nextFloat(): float = scanf("%lf", addr result)
# proc nextFloats(n: int): seq[float] =
#     var res = newSeq[float](n)
#     for i in 0..<n:
#         res[i] = nextFloat()
#     return res
proc nextChar(): char =
    while true:
        var c = getchar()
        if int(c) > int(' '):
            return c
# proc nextChars(h, w: int): seq[seq[char]] =
#     result = newSeqWith(h, newSeqWith(w, '.'))
#     for y in 0..<h:
#         for x in 0..<w:
#             result[y][x] = nextChar()
proc nextString(): string =
    var get = false
    result = ""
    while true:
        var c = getchar()
        if int(c) > int(' '):
            get = true
            result.add(c)
        else:
            if get: break
            get = false
# proc nextStrings(n: int): seq[string] =
#     for _ in 0..<n:
#         result.add(nextString())
proc scan[T](): T =
    when T is int:
        return nextInt()
    elif T is int64:
        return nextInt64()
    elif T is float:
        return nextFloat()
    elif T is char:
        return nextChar()
    elif T is string:
        return nextString()
    else:
        assert false

proc scans[T](n: int): seq[T] =
    result = newSeq[T](n)
    rep(i, n):
        result[i] = scan[T]()

proc scans[T](h, w: int): seq[seq[T]] =
    result = newSeqWith(h, newSeq[T](w))
    rep(y, h):
        rep(x, w):
            result[y][x] = scan[T]()

proc scanT[T](siz: static[Positive]): tuple =
    when siz == 1:
        return (scan[T](), )
    elif siz == 2:
        return (scan[T](), scan[T]())
    elif siz == 3:
        return (scan[T](), scan[T](), scan[T]())
    elif siz == 4:
        return (scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 5:
        return (scan[T](), scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 6:
        return (scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 7:
        return (scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 8:
        return (scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 9:
        return (
            scan[T](), scan[T](), scan[T](), scan[T](), scan[T](),
            scan[T](), scan[T](), scan[T](), scan[T]())
    elif siz == 10:
        return (
            scan[T](), scan[T](), scan[T](), scan[T](), scan[T](),
            scan[T](), scan[T](), scan[T](), scan[T](), scan[T]())
    else:
        assert false

proc scanTs[T](n: int, siz: static[Positive]): seq[tuple] =
    for _ in 0..<n:
        result.add(scanT[T](siz))

proc scan1[T1](): (T1, ) = (scan[T1](), )
proc scan1s[T1](n: int): seq[(T1, )] =
    result = newSeq[(T1, )](n)
    rep(i, n):
        result[i] = (scan[T1](), )
proc scan2[T1, T2](): (T1, T2) = (scan[T1](), scan[T2]())
proc scan2s[T1, T2](n: int): seq[(T1, T2)] =
    result = newSeq[(T1, T2)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2]())
proc scan3[T1, T2, T3](): (T1, T2, T3) = (scan[T1](), scan[T2](), scan[T3]())
proc scan3s[T1, T2, T3](n: int): seq[(T1, T2, T3)] =
    result = newSeq[(T1, T2, T3)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3]())
proc scan4[T1, T2, T3, T4](): (T1, T2, T3, T4) = (scan[T1](), scan[T2](), scan[T3](), scan[T4]())
proc scan4s[T1, T2, T3, T4](n: int): seq[(T1, T2, T3, T4)] =
    result = newSeq[(T1, T2, T3, T4)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4]())
proc scan5[T1, T2, T3, T4, T5](): (T1, T2, T3, T4, T5) = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5]())
proc scan5s[T1, T2, T3, T4, T5](n: int): seq[(T1, T2, T3, T4, T5)] =
    result = newSeq[(T1, T2, T3, T4, T5)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5]())
proc scan6[T1, T2, T3, T4, T5, T6](): (T1, T2, T3, T4, T5, T6) =
    (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6]())
proc scan6s[T1, T2, T3, T4, T5, T6](n: int): seq[(T1, T2, T3, T4, T5, T6)] =
    result = newSeq[(T1, T2, T3, T4, T5, T6)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6]())
proc scan7[T1, T2, T3, T4, T5, T6, T7](): (T1, T2, T3, T4, T5, T6, T7) =
    (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7]())
proc scan7s[T1, T2, T3, T4, T5, T6, T7](n: int): seq[(T1, T2, T3, T4, T5, T6, T7)] =
    result = newSeq[(T1, T2, T3, T4, T5, T6, T7)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7]())
proc scan8[T1, T2, T3, T4, T5, T6, T7, T8](): (T1, T2, T3, T4, T5, T6, T7, T8) =
    (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7](), scan[T8]())
proc scan8s[T1, T2, T3, T4, T5, T6, T7, T8](n: int): seq[(T1, T2, T3, T4, T5, T6, T7, T8)] =
    result = newSeq[(T1, T2, T3, T4, T5, T6, T7, T8)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7](), scan[T8]())
proc scan9[T1, T2, T3, T4, T5, T6, T7, T8, T9](): (T1, T2, T3, T4, T5, T6, T7, T8, T9) =
    (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7](), scan[T8](), scan[T9]())
proc scan9s[T1, T2, T3, T4, T5, T6, T7, T8, T9](n: int): seq[(T1, T2, T3, T4, T5, T6, T7, T8, T9)] =
    result = newSeq[(T1, T2, T3, T4, T5, T6, T7, T8, T9)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7](), scan[T8](), scan[T9]())
proc scan10[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10](): (T1, T2, T3, T4, T5, T6, T7, T8, T9, T10) =
    (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](), scan[T6](), scan[T7](), scan[T8](), scan[T9](), scan[T10]())
proc scan10s[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10](n: int): seq[(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)] =
    result = newSeq[(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)](n)
    rep(i, n):
        result[i] = (scan[T1](), scan[T2](), scan[T3](), scan[T4](), scan[T5](),
            scan[T6](), scan[T7](), scan[T8](), scan[T9](), scan[T10]())

iterator range(stop: int): int =
    for x in 0..<stop:
        yield x

iterator range(start: int, stop: int): int =
    for x in start..<stop:
        yield x

iterator range(start: int, stop: int, step: int): int =
    assert step != 0
    if step > 0:
        var a = start
        while a < stop:
            yield a
            a += step
    else:
        var a = start
        while a > stop:
            yield a
            a += step

func `+`(a, b: bool): bool = a or b
func `*`(a, b: bool): bool = a and b
func `+`(a: var bool, b: bool): bool = a = a or b
func `*`(a: var bool, b: bool): bool = a = a and b
func `|`(a, b: bool): bool = a or b
proc `|=`(a: var bool, b: bool) =
    a = a or b
func `|`(c1, c2: int): int = c1 or c2

func `&`(a, b: bool): bool = a and b
proc `&=`(a: var bool, b: bool) =
    a = a and b
func `&`(c1, c2: int): int = c1 and c2

func `//`(a, b: int): int = a div b

proc `//=`(a: var int, b: int) =
    a = a div b

func `%`(a: int, b: Positive): int =
    result = a mod b
    if result < 0:
        result += b

proc `%=`(a: var int, b: int) =
    a = a mod b

func `<<`(a: int, s: int): int = a shl s
func `>>`(a: int, s: int): int = a shr s
proc `>>=`(a: var int, b: int) =
    a = a shr b
proc `[]`(n, i: Natural): int = (n >> i) & 1

func ceil[T: SomeInteger](b, a: T): T = (a+b-1) div a
func all(a: openArray[bool]): bool =
    result = true
    for x in a:
        result &= x

func any(a: openArray[bool]): bool =
    result = false
    for x in a:
        result |= x

# Z/mo Z上のaの逆元
func inv(a, mo: int): int =
    var r = (mo, a)
    var x = (0, 1)
    while r[1] != 0:
        x = (x[1], (x[0] - (r[0] div r[1]) * x[1]) % mo)
        r = (r[1], r[0] % r[1])

    if r[0] == 1:
        return x[0]
    else:
        return -1

func bitLength(n: Natural): int =
    result = 0
    var num = abs(n)
    while num > 0:
        result += 1
        num = num shr 1

proc chmax[T](a: var T, vars: varargs[T]): bool {.discardable.} =
    let target = max(vars)
    if a < target:
        a = target
        return true
    else:
        return false

proc chmin[T](a: var T, vars: varargs[T]): bool {.discardable.} =
    let target = min(vars)
    if a > target:
        a = target
        return true
    else:
        return false

template present(a: typed): bool = a.len != 0
template empty(a: typed): bool = a.len == 0

template loop(body: untyped): untyped =
    while true:
        body

func Yn(cond: bool, yes: string = "Yes", no: string = "No"): string =
    if cond:
        yes
    else:
        no

func `<`[T](u, v: openArray[T]): bool =
    assert u.len == v.len
    for j in 0..<u.len:
        if u[j] != v[j]:
            return u[j]-v[j] < 0

    return true

template pass() = discard
template pass(x: typed) = discard x
template cont() = continue
template ret() = return

# start coding
