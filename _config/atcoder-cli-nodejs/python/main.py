#!/usr/bin/env pypy3

import re
from collections import deque
from inspect import currentframe
from pprint import pprint
from string import ascii_lowercase as letter
from string import ascii_uppercase as LETTER
from sys import set_int_max_str_digits, setrecursionlimit, stderr, stdin
from time import sleep as _time_sleep
from typing import Dict, Generic, Iterable, Iterator, List, Optional, Set, Tuple, TypeVar, Union

try:
    import pypyjit
    pypyjit.set_param('max_unroll_recursion=-1')
except ModuleNotFoundError:
    ...

INF: int = (1 << 62) - 1
MOD1000000007 = 10**9 + 7
MOD998244353 = 998244353
setrecursionlimit(10_000_000)  # 10**7
set_int_max_str_digits(0)  # 4300桁制限を解除する
readline = stdin.readline
input = lambda: stdin.readline().rstrip('\r\n')


def copy2d(a: list) -> list:
    return [[y for y in x] for x in a]


def copy3d(a: list) -> list:
    return [[[z for z in y] for y in x] for x in a]


def print2d(A):
    for x in A:
        print(*x)


def flattern2d(mat: list) -> list:
    return [x for a in mat for x in a]


IS_DEVELOPMENT = None


def debug(*a) -> None:
    global IS_DEVELOPMENT
    if IS_DEVELOPMENT is None:
        IS_DEVELOPMENT = __file__.endswith('/main.py')

    if not IS_DEVELOPMENT:
        return

    line_no = currentframe().f_back.f_lineno
    print(f"L{line_no}:", *a, file=stderr)


def inputs(type_=int, one_word=False) -> list:
    in_ = input()
    if one_word:
        ins = list(in_)
    else:
        ins = in_.split()

    if isinstance(type_, Iterable):
        return [t(x) for t, x in zip(type_, ins)]
    else:
        return list(map(type_, ins))


def inputi() -> int:
    return int(readline())


yn = ['no', 'yes']
Yn = ['No', 'Yes']
YN = ['NO', 'YES']

# start coding
